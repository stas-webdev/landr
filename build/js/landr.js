
(function (window, $) {
  var Landr = window.Landr = {};

  Landr.init = function (options) {
    initCountdown(options.countdown);
    if (options.parallax) initParallax();
    initWowAnimations();
  }

  function initCountdown (options) {
    var $el = $(options.el);
    $el.countdown(options.date, function (e) {
      $el.find('.countdown-unit--days .countdown-unit-value').text(e.offset.totalDays);
      $el.find('.countdown-unit--hours .countdown-unit-value').text(e.offset.hours);
      $el.find('.countdown-unit--minutes .countdown-unit-value').text(e.offset.minutes);
      $el.find('.countdown-unit--seconds .countdown-unit-value').text(e.offset.seconds);
    });
  }

  function initParallax() {
    $(window).stellar({ horizontalScrolling: false, responsive: true, hideDistantElements: true });
  }

  function initWowAnimations () {
    new WOW().init();
  }

})(window, $);
