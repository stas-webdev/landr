"use strict";

const gulp = require('gulp');
const jade = require('gulp-jade');
const less = require('gulp-less');
const LessAutoprefix = require('less-plugin-autoprefix');

/***** Jade *****/

gulp.task('jade:compile', function () {
  return gulp.src('./jade/*.jade')
    .pipe( jade( { pretty: true } ) )
    .pipe(gulp.dest('./build'))
});

gulp.task('jade:watch', function () {
  return gulp.watch('./jade/**/*.*', ['jade:compile']);
});

/***** JS *****/

gulp.task('js:compile', function () {
  return gulp.src('./js/*.*')
    .pipe(gulp.dest('./build/js'));
});

gulp.task('js:watch', function () {
  return gulp.watch('./js/**/*.*', ['js:compile']);
});

/***** LESS *****/

gulp.task('less:compile', function () {
  let autoprefix = new LessAutoprefix({ browsers: ['last 2 versions'] });
  return gulp.src('./less/styles.less')
    .pipe(less({ plugins: [ autoprefix ] }))
    .pipe(gulp.dest('./build/css/'));
});

gulp.task('less:watch', function () {
  return gulp.watch('./less/**/*.*', ['less:compile']);
});

/***** Vendor *****/

gulp.task(
  'vendor:compile',
  ['vendor:jquery', 'vendor:bootstrap', 'vendor:countdown',
    'vendor:fontawesome', 'vendor:stellar', 'vendor:smoothscroll',
    'vendor:animatecss', 'vendor:wow']
);

gulp.task('vendor:jquery', function () {
  return gulp.src('./bower_components/jquery/dist/*.*')
    .pipe(gulp.dest('./build/vendor/jquery'))
});

gulp.task('vendor:bootstrap', function () {
  return gulp.src('./bower_components/bootstrap/dist/**/*.*')
    .pipe(gulp.dest('./build/vendor/bootstrap'));
});

gulp.task('vendor:countdown', function () {
  return gulp.src('./bower_components/jquery.countdown/dist/*.*')
    .pipe(gulp.dest('./build/vendor/jquery.countdown'));
});

gulp.task('vendor:fontawesome', ['vendor:fontawesome:css', 'vendor:fontawesome:fonts']);

gulp.task('vendor:fontawesome:css', function () {
  return gulp.src('./bower_components/font-awesome/css/*.*')
    .pipe(gulp.dest('./build/vendor/font-awesome/css'));
});

gulp.task('vendor:fontawesome:fonts', function () {
  return gulp.src('./bower_components/font-awesome/fonts/*.*')
    .pipe(gulp.dest('./build/vendor/font-awesome/fonts'));
});

gulp.task('vendor:stellar', function () {
  return gulp.src('./bower_components/jquery.stellar/jquery.stellar.*')
    .pipe(gulp.dest('./build/vendor/jquery.stellar'));
});

gulp.task('vendor:smoothscroll', function () {
  return gulp.src('./bower_components/smoothscroll-for-websites/SmoothScroll.js')
    .pipe(gulp.dest('./build/vendor/smoothscroll'));
});

gulp.task('vendor:animatecss', function () {
  return gulp.src('./bower_components/animate.css/animate*.css')
    .pipe(gulp.dest('./build/vendor/animate.css'));
});

gulp.task('vendor:wow', function () {
  return gulp.src('./bower_components/wow/dist/*.*')
    .pipe(gulp.dest('./build/vendor/wow'));
});

/***** Assets *****/
gulp.task('assets:compile', ['assets:img']);

gulp.task('assets:watch', function () {
  return gulp.watch('./assets/**/*.*', ['assets:compile']);
});

gulp.task('assets:img', function () {
  return gulp.src('./assets/img/**/*.*')
    .pipe(gulp.dest('./build/img/'));
});

/***** Build & Watch & Default *****/

gulp.task('build', ['jade:compile', 'js:compile', 'less:compile', 'vendor:compile', 'assets:compile']);
gulp.task('watch', ['jade:watch', 'js:watch', 'less:watch']);
gulp.task('default', ['build']);
